﻿#include <iostream>
#include <ctime>

using namespace std;

int main()
{
    setlocale(0, "Russian");

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    const int COLUMN = 5;
    const int ROW = 5;
    int arr[ROW][COLUMN];
    int sum[ROW] = { 0 };

    cout << "Это двумерный массив "<< ROW<<"x"<<COLUMN<<endl<<endl;

    for (int i = 0; i < ROW;i++)
    {
        for (int j = 0; j < COLUMN; j++)
        {
            arr[i][j] = i + j;
            sum[i] += arr[i][j];
            cout.width(3);
            cout << arr[i][j];
        }
        cout << endl;
    }
    cout << endl;
    cout << "Программа выводит сумму чисел строки массива вверху под номером\nравным остатку деления сегодняшней даты на указанное вами число.\n\n";
    cout << "Введите число: ";
    int n;
    cin >> n;
    int a = buf.tm_mday % n;
    cout << "Сегодняшняя дата " << buf.tm_mday << '/' << (buf.tm_mon + 1) << '/'
        << (buf.tm_year + 1900) << ", "<< "вы ввели число "<< n <<", следовательно сумма чисел строки "<<a<< " = "<<sum[a]<<endl;
}